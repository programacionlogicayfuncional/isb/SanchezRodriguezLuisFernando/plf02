(ns plf02.core)

;Funciones Predicados

(defn función-associative?-1
  [a]
  (associative? a))

(defn función-associative?-2
  [a]
  (associative? a ))

(defn función-associative?-3
  [a]
  (associative? a))

(función-associative?-1 '(1 2 3) )
(función-associative?-2 [1 2 3])
(función-associative?-3 {:a 1 :b 2 :c 3})

(defn función-boolean?-1
  [a]
  (boolean? a))

(defn función-boolean?-2
  [a]
  (boolean? a))


(defn función-boolean?-3
  [a]
  (boolean? a))

(función-boolean?-1 ((fn [a] (pos? a)) 20) ) 
(función-boolean?-2 ((fn [a] (vector? a)) [1 2 3 4]))
(función-boolean?-3 3)

(defn función-char?-1
  [a]
  (char? a))

(defn función-char?-2
  [a]
  (char? a))

(defn función-char?-3
  [a]
  (char? a))

(función-char?-1 \a)
(función-char?-2 ["hola" "123"])
(función-char?-3 "Fernando")

(defn función-coll?-1
  [a]
  (coll? a))


(defn función-coll?-2
  [a]
  (coll? a))


(defn función-coll?-3
  [a]
  (coll? a))

(función-coll?-1 [ 1 2 3])
(función-coll?-2 (range 1 20))
(función-coll?-3 {:a 1 :b 1 :c 2})

(defn función-decimal?-1 
  [a]
  (decimal? a))

(defn función-decimal?-2
  [a ]
  (decimal? a))

(defn función-decimal?-3
  [a]
  (decimal? a))

(función-decimal?-1 10.9M)
(función-decimal?-2 297)
(función-decimal?-3 0.1M)

(defn función-double?-1
  [a]
  (double? a))

(defn función-double?-2
  [a]
  (double? a))

(defn función-double?-3
  [a]
  (double? a))

(función-double?-1 1.0)
(función-double?-2 "1")
(función-double?-3 50)

(defn función-float?-1
  [a]
  (float? a))

(defn función-float?-2
  [a]
  (float? a))

(defn función-float?-3
  [a]
  (float? a))

(función-float?-1 1.0)
(función-float?-2 2.0)
(función-float?-3 1 )

(defn función-ident?-1
  [a]
  (ident? a))

(defn función-ident?-2
  [a]
  (ident? a))

(defn función-ident?-3
  [a]
  (ident? a))

(función-ident?-1 'abc)
(función-ident?-2 :hola)
(función-ident?-2 123)

(defn función-indexed?-1
 [a]
 (indexed? a) )

(defn función-indexed?-2
  [a]
  (indexed? a))


(defn función-indexed?-3
  [a]
  (indexed? a))

(función-indexed?-1 {:a 1 :b 2})
(función-indexed?-2 [1 2 3 4])
(función-indexed?-3 1)

(defn función-int?-1
  [a]
  (int? a))

(defn función-int?-2
  [a]
  (int? a))

(defn función-int?-3
  [a ]
  (int? a ))

(función-int?-1 2)
(función-int?-2 2.0)
(función-int?-3 [4.8 2 1])

(defn función-integer?-1
  [a]
  (integer? a))

(defn función-integer?-2
  [a]
  (integer? a))

(defn función-integer?-3
  [a ]
  (integer? a))

(función-integer?-1 2)
(función-integer?-2 2.0)
(función-integer?-3 [21  132])

(defn función-keyword?-1
  [a]
  (keyword? a))

(defn función-keyword?-2
  [a]
  (keyword? a))

(defn función-keyword?-3
  [a]
  (keyword? a))

(función-keyword?-1 :a)
(función-keyword?-2 [:a \a :b \b :c \c :e \e])
(función-keyword?-3 :hola)

(defn función-list?-1
  [a ]
  (list? a))

(defn función-list?-2
  [a ]
  (list? a))

(defn función-list?-3
  [a]
  (list? a))

(función-list?-1 {[1 5] [2 2] })
(función-list?-2 [287 354])
(función-list?-3 (range 50))

(defn función-map-entry?-1
  [a]
  (map-entry? a))

(defn función-map-entry?-2
  [a]
  (map-entry? a))

(defn función-map-entry?-3
  [a]
  (map-entry? a))

(función-map-entry?-1 {:a 1 :b 2})
(función-map-entry?-2 {:hola "Hola" :mundo "Mundo"})
(función-map-entry?-3 [1 2 2])

(defn función-map?-1
  [a]
  (map? a))

(defn función-map?-2
  [a]
  (map? a))

(defn función-map?-3
  [a]
  (map? a))

(función-map?-1 {:a 1 :b 2} )
(función-map?-2 1)
(función-map?-3 "Hola")

(defn función-nat-int?-1
  [a]
  (nat-int? a))

(defn función-nat-int?-2
  [a]
  (nat-int? a))

(defn función-nat-int?-3
  [a]
  (nat-int? a))

(función-nat-int?-1 20)
(función-nat-int?-2 -2)
(función-nat-int?-3 9.76 )

(defn función-number?-1
  [a]
  (number? a))

(defn función-number?-2
  [a]
  (number? a))

(defn función-number?-3
  [a]
  (number? a))

(función-number?-1 1)
(función-number?-2 \a)
(función-number?-3 52.12)

(defn función-pos-int?-1
  [a]
  (pos-int? a))

(defn función-pos-int?-2
  [a]
  (pos-int? a))

(defn función-pos-int?-3
  [a]
  (pos-int? a))

(función-pos-int?-1 1231245125412512)
(función-pos-int?-2 1.976)
(función-pos-int?-3 65M)


(defn función-ratio?-1
  [a]
  (ratio? a)
  )

(defn función-ratio?-2
  [a]
  (ratio? a))

(defn función-ratio?-3
 [a]
  (ratio? a)
  )

(función-ratio?-1 1/5)
(función-ratio?-2 10/20)
(función-ratio?-3 1/2 )

(defn función-rational?-1
  [a]
  (rational? a))

(defn función-rational?-2
  [a]
  (rational? a))

(defn función-rational?-3
  [a]
  (rational? a))

(función-rational?-1 200)
(función-rational?-2 95.16)
(función-rational?-3 51.05)

(defn función-seq?-1
  [a]
  (seq? a))

(defn función-seq?-2
  [a]
  (seq? a))

(defn función-seq?-3
  [a]
  (seq? a))

(función-seq?-1 200)
(función-seq?-2 [1 23])
(función-seq?-3 ['(1 2 3)])

(defn función-seqable?-1
  [a]
  (seqable? a))

(defn función-seqable?-2
  [a]
  (seqable? a))

(defn función-seqable?-3
  [a]
  (seqable? a))

(función-seqable?-1 [])
(función-seqable?-2 {})
(función-seqable?-3 "")

(defn función-sequential?-1
  [a]
  (sequential? a))

(defn función-sequential?-2
  [a]
  (sequential? a))

(defn función-sequential?-3
  [a]
  (sequential? a))

(función-sequential?-1 '(20 21 22 23))
(función-sequential?-2 20)
(función-sequential?-3 {:a 1 :b 2 :c 4 })

(defn función-set?-1
  [a]
  (set? a))

(defn función-set?-2
  [a]
  (set? a))


(defn función-set?-3
  [a]
  (set? a))

(función-set?-1 {:a 1 :b 2 :c 3})
(función-set?-2 (hash-set 1 2 3))
(función-set?-3 #{1 2 3 4 5})

(defn función-some?-1
  [a]
  (some? a))

(defn función-some?-2
  [a]
  (some? a))

(defn función-some?-3
  [a]
  (some? a))

(función-some?-1 nil)
(función-some?-2 true)
(función-some?-3 [])

(defn función-string?-1
  [a]
  (string? a))

(defn función-string?-2
  [a]
  (string? a))

(defn función-string?-3
  [a]
  (string? a))

(función-string?-1 "Hola")
(función-string?-2 ["ABC" 2 4])
(función-string?-3 1)

(defn función-symbol?-1
  [a]
  (symbol? a))

(defn función-symbol?-2
  [a]
  (symbol? a))

(defn función-symbol?-3
  [a]
  (symbol? a))

(función-symbol?-1 'a)
(función-symbol?-2 [\a "g"])
(función-symbol?-3 1)

(defn función-vector?-1
  [a]
  (vector? a))

(defn función-vector?-2
  [a]
  (vector? a))

(defn función-vector?-3
  [a]
  (vector? a))

(función-vector?-1 [1 2 3])
(función-vector?-2 '(1 2 33 4))
(función-vector?-3 {:a "a" :b "b" :c "c" :d "d"})

; Funciones de Orden Superior

(defn función-drop-1
  [a b]
  (drop  a b))

(defn función-drop-2
  [a b]
  (drop a b))

(defn función-drop-3
  [a b]
  (drop a b))

(función-drop-1 2 [1 2 3 4 5 6 7 8 9 10 11])
(función-drop-2  5 [1 2 3 4 5 6 7 8 9 10])
(función-drop-3 1 [1 2 3 4])

(defn función-drop-last-1
  [a b]
  (drop-last a b))

(defn función-drop-last-2
  [a b]
  (drop-last a b))

(defn función-drop-last-3
  [a b]
  (drop-last a b))

(función-drop-last-1 3 [1 2 3 4 5 6])
(función-drop-last-2 4 [76124 82718792 41792 791 21489])
(función-drop-last-3 2 {:b 2 :c 3 :d 4 :e 7})

(defn función-drop-while-1
  [a b]
  (drop-while a b))

(defn función-drop-while-2
  [a b]
 (drop-while a b))

(defn función-drop-while-3
  [a b]
  (drop-while a b) )

(función-drop-while-1 #(== 6 %) [-1 -2 -3 -400 5 6 -123 -5321 -65 23 54 754])
(función-drop-while-2 neg? [-3 -3 2 -1 -86 31 645 3 3])
(función-drop-while-3 #(== 0 %) [3 3 2 1 86 31 645 3 3])

(defn función-every?-1
  [a b]
  (every? a b))

(defn función-every?-2
  [a b]
  (every? a b))

(defn función-every?-3
  [a b]
  (every? a b))

(función-every?-1 even? '(6 12 18))
(función-every?-2 #{20 30} [20 30])
(función-every?-3 vector? [])

(defn función-filterv?-1
  [a b]
  (filterv a b))

(defn función-filterv?-2
  [a b]
  (filterv a b))

(defn función-filterv?-3
  [a b]
  (filterv a b))

(función-filterv?-1 pos-int? (range 10))
(función-filterv?-2  neg? (range -5 1))
(función-filterv?-3  odd? (range 20))

(defn función-group-by?-1
  [a b]
  (group-by a b))

(defn función-group-by?-2
  [a b]
  (group-by a b))

(defn función-group-by?-3
  [a b]
  (group-by a b))

(función-group-by?-1 pos? (range 10) )
(función-group-by?-2 count ["aa" "cc" "dd" "ee"])
(función-group-by?-3 neg? (range -10 10))

(defn función-iterate-1
  [a b]
  (take 3 (iterate a b)))

(defn función-iterate-2
  [a b]
  (take 8 (iterate a b)))

(defn función-iterate-3
  [a b]
  (take 9 (iterate a b)))

(función-iterate-1 inc 5)
(función-iterate-2 dec 5)
(función-iterate-3 inc 2)

(defn función-keep-1
  [a b]
  (keep a b))

(defn función-keep-2
  [a b]
  (keep a b))

(defn función-keep-3
  [a b]
  (keep a b))


(función-keep-1 #{0 1 2 3} #{2 3 4 5})
(función-keep-2 pos? (range 1 9))
(función-keep-3 #{"zxc" "fas" "asd"} #{1 32 421}) 


(defn función-map-indexed-1
  [a b]
  (map-indexed a b))

(defn función-map-indexed-2
  [a b]
  (map-indexed a b))

(defn función-map-indexed-3
  [a b]
  (map-indexed a b))

(función-map-indexed-1 list "Hola")
(función-map-indexed-2 hash-map "Buenas noches")
(función-map-indexed-3 vector"Buenas noches")

(defn función-mapcat-1
  [a b]
  (mapcat a b))

(defn función-mapcat-2
  [a b]
  (mapcat a b))

(defn función-mapcat-3
  [a b]
  (mapcat a b))

(función-mapcat-1 reverse [[1 2 3] [8 9 0] [7 5 2]])
(función-mapcat-2 #(repeat 2 %) [1 2 3 4 5])
(función-mapcat-3 reverse [["a" "t" " s" "h"]["hola"]["rubí"]])

(defn función-mapv-1
  [a b c]
  (mapv a b c))

(defn función-mapv-2
  [a b ]
  (mapv a b))

(defn función-mapv-3
  [a b c ]
  (mapv a b c))

(función-mapv-1 + [20 78 99]  [42 59 102])
(función-mapv-2  inc [201 41 51 128 4982 12] ) 
(función-mapv-3 + [20 30 41 21] [100 200 310] )

(defn función-merge-with-1
  [a b c]
  (merge-with a b c))

(defn función-merge-with-2
  [a b c ]
  (merge-with a b c))

(defn función-merge-with-3
  [a b c]
  (merge-with a b c))

(función-merge-with-1 + {:a 4 :b 3} {:a 123 :b 42 :c 0})
(función-merge-with-2 * {:a 41 :b 31} {:c 511 :d 38})
(función-merge-with-3 merge {:a {:a 412}}{:b {:x 241}} )

(defn función-not-any?-1
  [a b]
  (not-any? a b))

(defn función-not-any?-2
  [a b]
  (not-any? a b))

(defn función-not-any?-3
  [a b]
  (not-any? a b))

(función-not-any?-1 odd? '(1 2 3))
(función-not-any?-2 nil? '(20 30 789))
(función-not-any?-3 boolean? '(true false false))


(defn función-not-every?-1
  [a b]
  (not-every? a b))

(defn función-not-every?-2
  [a b]
  (not-every? a b))

(defn función-not-every?-3
  [a b]
  (not-every? a b))


(función-not-every?-1 pos? '(-1 5 9 10 false))
(función-not-every?-2 odd? '(1 2 3 4 5 6))
(función-not-every?-3 boolean? '(true false false true))

(defn función-partition-by-1
  [a b]
  (partition-by a b))

(defn función-partition-by-2
  [a b]
  (partition-by a b))

(defn función-partition-by-3
  [a b]
  (partition-by a b))


(función-partition-by-1 odd? [88888 7777 888 5555 66666 3333 222 10101010])
(función-partition-by-2 identity "fffeerrrrnnnnnaaaadddoo")
(función-partition-by-3 pos? [-1 -2 -43 -533 123 3  45 ])


(defn función-reduce-kv-1
  [a b c]
  (reduce-kv  a b c))


(defn función-reduce-kv-2
  [a b c]
  (reduce-kv  a b c))

(defn función-reduce-kv-3
  [a b c]
  (reduce-kv a b c))

(función-reduce-kv-1  vector [{:a 1 :b 2}] [{:d 2 :b 4}])
(función-reduce-kv-2 assoc {} {:a 1 :b 3 :c 3})
(función-reduce-kv-3 vector [{:a -501 :b 45}] [{:a 7989 :b -301}])

(defn función-remove-1
  [a b]
  (remove a b))

(defn función-remove-2
  [a b]
  (remove a b))

(defn función-remove-3
  [a b]
  (remove a b))

(función-remove-1 pos? [1 -2 -3 4 5 -6])
(función-remove-2 float? [1.0 -117.97 -3 4 523.92 -6826])
(función-remove-3 even? (range 20))

(defn función-reverse-1
  [a]
  (reverse a))

(defn función-reverse-2
  [a]
  (reverse a))

(defn función-reverse-3
  [a]
  (reverse a))

(función-reverse-1 [1 2 3 4])
(función-reverse-2 "Fernando")
(función-reverse-3 [true true false false false false true false])


(defn función-some-1
  [a b]
  (some a b))

(defn función-some-2
  [a b]
  (some a b))

(defn función-some-3
  [a b]
  (some a b))

(función-some-1 even? [1 3 5 7 9 11])
(función-some-2 pos? [-1 -2 -4 66 -7 65 -4 -3 -2])
(función-some-3 true? [true false false false true true])

(defn función-sort-by-1
  [a b]
  (sort-by a b))

(defn función-sort-by-2
  [a b]
  (sort-by a b))

(defn función-sort-by-3
  [a b]
  (sort-by a b))

(función-sort-by-1 first ["Hola" "Mundo" "Coljure" "Lein"])
(función-sort-by-2 count '([4 12] [45 34 1] [456 123]))
(función-sort-by-3 last {\a \b \c \C \b \B \t \V})



(defn función-split-with-1
  [a b]
   (split-with a b))

(defn función-split-with-2
  [a b]
  (split-with  a b))

(defn función-split-with-3
  [a b]
  (split-with  a b))


(función-split-with-1 (partial >= 30) [10 20 30 40 50 60 70 80 90 100])
(función-split-with-2 (partial > 3) [1 2 3 4 5 6 7 8 9 10])
(función-split-with-3 (partial == 13) [11 12 14 13 15 16 17 18])

(defn función-take-1
  [a b]
  (take a b))

(defn función-take-2
  [a b]
  (take  a b))

(defn función-take-3
  [a b]
  (take a b))

(función-take-1 5 [1 2 3 4 5 6 7 8])
(función-take-2 3 [ -1 -2 -3 -4 5 3])
(función-take-3 4 [3 3 1 2 3 4 5 1 12 3])

(defn función-take-last-1
  [a b]
  (take-last a b))

(defn función-take-last-2
  [a b]
  (take-last a b))

(defn función-take-last-3
  [a b]
  (take-last a b))

(función-take-last-1 2 [19 29 83 281 2 1])
(función-take-last-2 3 [14214 124812 7216 48712627 9 87 10])
(función-take-last-3 5 [123 213 4 32 1 5 12 123 41])

(defn función-take-nth-1
  [a b]
  (take-nth a b ))

(defn función-take-nth-2
  [a b]
  (take-nth a b))

(defn función-take-nth-3
  [a b]
  (take-nth a b))

(función-take-nth-1 2 (range 20))
(función-take-nth-2 3 (range 40))
(función-take-nth-3 5 (range 50))

(defn función-take-while-1
  [a b]
  (take-while a b))

(defn función-take-while-2
  [a b]
  (take-while a b))

(defn función-take-while-3
  [a b]
  (take-while a b))


(función-take-while-1 pos? [1 2 3 -4])
(función-take-while-2 neg? [-1 -2 -3 4 1 21 412 124 ])
(función-take-while-3 boolean? [true false true true 1 23 31 false ])

(defn función-update-1
  [a b c]
  (update a b c))

(defn función-update-2
  [a b c]
  (update a b c))

(defn función-update-3
  [a b c]
  (update a b c))

(función-update-1 [1 2 3] 0 inc)
(función-update-2 [90 16 29 3] 1 dec) 
(función-update-3 [1111 2222 3333 4444 ] 3 inc)

(defn función-update-in-1
  [a b c]
  (update-in a b c ))

(defn función-update-in-2
  [a b c d]
  (update-in a b c d))

(defn función-update-in-3
  [a b c d]
  (update-in a b c d))

(función-update-in-1 {:name "Fernando" :age 23} [:age] inc)
(función-update-in-2 {:name "Fernando" :age 23} [:age] + 10 )
(función-update-in-3 {:name "Fernando" :age 23} [:age] - 10)
